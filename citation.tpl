<?php
/*
 * article citation template
 * 
 * This template will be used if there is no template defined by a citation plugin
 */
?>
<div<?php print $citation_attributes ?>>
  <?php if ($access): ?>
    <div<?php print $access_attributes ?>><?php print $access; ?></div>
  <?php endif; ?>

  <?php if ($title): ?>
        <?php print $title; ?>
  <?php endif; ?>

  <?php if ($highlight): ?>
    <div<?php print $highlight_attributes ?>><?php print $highlight; ?></div>
  <?php endif; ?>

  <?php if ($snippet): ?>
  	<div<?php print $snippet_attributes ?>><?php print $snippet; ?></div>
  <?php endif; ?>
  
  <?php if ($authors): ?>
  	<div<?php print $authors_attributes ?>><?php print $authors; ?></div>
  <?php endif; ?>

  <?php if ($metadata): ?>
  	<div<?php print $metadata_attributes ?>><?php print $metadata; ?></div>
  <?php endif; ?>

  <?php if ($extras): ?>
  	<div<?php print $extras_attributes ?>><?php print $extras; ?></div>
  <?php endif; ?>
</div>
