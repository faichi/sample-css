================================
README.md
================================

We use Omega as the base theme for any theme Development.
All themes are tested with latest browser versions for IE, FF, Chrome and Safari. 

Here are some examples for key areas :

1) MARKUP: - We write every template per Drupal coding standards and try to go close to MVC much as possible. 
a) We use module files for preparing variables and defining templates for elements e.g. contact form. 
b) We use .tpl files containing HTML markup and printing PHP variables.

Example file: citation.tpl 

2) UI :- We use jquery /jquery-ui for any kind of interactive behaviors.
Example file: js-example.js

3) CSS Design: We normally use CSS3 with LESS / SCSS . Color options are kept configurable in Drupal admin area.
e.g. links colors, button colors, background colors etc.
Example file: _footer.scss


4) Responsive layouts: - Most of time we use responsive designs and use one of the grid system. e.g omega, bootstrap.
Example file: responsive.css